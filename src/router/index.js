import Vue from 'vue';
import VueRouter from 'vue-router';
import Crude from "../components/Crude"
import List from "../components/List";



Vue.use(VueRouter);

const routes =[
  {
    path: "/",
    name: "Crude",
    component: Crude,

  },
  {
    path: "/list",
    name: "List",
    component: List,
  }


];

const router = new VueRouter({
  routes
});





export default router;
